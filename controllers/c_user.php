<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * summary
 */
class C_user extends CI_Controller
{
    /**
     * summary
     */
    public function __construct(){
    	parent::__construct();
    	$this->load->helper(array('form','url','date'));
    	$this->load->database();
    	$this->load->model(array('data_model'));
        $this->load->library('pdf');
		//$this->load->library();
    }
    function upload_bukti_cicilan($id)
	{
		$id_user=$this->session->userdata('nomor_registrasi');
		$konfirmasi_tagihan=$this->data_model->edit_data_siswa('id_konfirmasi',$id,'tb_konfirmasi_tagihan');
		foreach ($konfirmasi_tagihan->result() as $value){
			$idkonfirmasi=$value->id_konfirmasi;
			$idpendidikan=$value->id_pendidikan;
	}
		$simpan['id_tagihan']="";
		$simpan['id_cicilan']=$idkonfirmasi;
		$simpan['total_bayar']=$this->input->post('jumlah');
		$simpan['no_registrasi']=$id_user;
		$simpan['id_pendidikan']=$idpendidikan;
		$simpan['tgl_bayar']=date('dmY');
		$config['upload_path'] = './tes_foto/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
			$result = array('upload_data' => $this->upload->data());
		}
		$simpan['bukti_bayar']=$result['upload_data']['file_name'];
        //print_r($simpan);
		$simpan_data['status_konfirmasi']="Sedang Di Cicil";
		$this->data_model->Update_status_cicil($id);
        $this->data_model->Simpan('tb_bayar_tagihan',$simpan);
		redirect(base_url('index.php/c_user/bayar_tagihan'));
	}
    function invoice_pdf($id)
    {

    }
    function invoice_print($id)
    {
        $nomor_registrasi=$this->session->userdata('nomor_registrasi');
        $data['paket_pendidikan']=$this->data_model->Paket_dipilih($id);
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        //$this->load->view('user/header');
        $this->load->view('user/invoice_pdf',$data);
        //$this->load->view('user/footer');
    }
    function simpan_edit_data_diri()
    {
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = array('upload_data' =>$this->upload->data());
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            $no_registrasi=$this->session->userdata('nomor_registrasi');
            $media['id_media']=$this->data_model->no_media_otomatis();
            $media['jenis_media']='foto_siswa'.$no_registrasi;
            $media['media']=$result['upload_data']['file_name'];
            $siswa['no_registrasi']=$no_registrasi;
            $siswa['jenis_kelamin']=$this->input->post('jenis_kelamin');
            $siswa['tempat_lahir']=$this->input->post('tempat_lahir');
            $siswa['tgl_lahir']=$this->input->post('tgl_lahir');
            $siswa['alamat']=$this->input->post('alamat');
            $siswa['tlp']=$this->input->post('telp');
            $siswa['whatsapp']=$this->input->post('whatsapp');
            $siswa['email']=$this->input->post('email');
            $siswa['bbm']=$this->input->post('bbm');
            $siswa['foto']=$media['id_media'];
            $sekolah['no_registrasi']=$no_registrasi;
            $sekolah['nm_sekolah']=$this->input->post('nama_sekolah');
            $sekolah['alamat_sekolah']=$this->input->post('alamat_sekolah');
            $sekolah['kelas']=$this->input->post('kelas');
            $ortu['no_registrasi']=$no_registrasi;
            $ortu['nama_ayah']=$this->input->post('nm_ayah');
            $ortu['pekerjaan_ayah']=$this->input->post('pk_ayah');
            $ortu['nama_ibu']=$this->input->post('nm_ibu');
            $ortu['pekerjaan_ibu']=$this->input->post('pekerjaan_ibu');
            $ortu['alamat_orangtua']=$this->input->post('alamat_orangtua');
            $ortu['hp']=$this->input->post('tlp_ortu');
            $ortu['Whatsapp_ortu']=$this->input->post('Whatsapp_ortu');
            $status_registrasi="Sudah";
            $this->data_model->update_status_registrasi('tb_user',$no_registrasi,$status_registrasi);
            $this->data_model->Simpan_Siswa('tb_siswa',$siswa);
            $this->data_model->Simpan_Sekolah('tb_sekolah',$sekolah);
            $this->data_model->Simpan_Ortu('tb_orangtua',$ortu);
            $this->data_model->upload_media('tb_media',$media);
            $this->data_model->simpan_edit_data_diri($no_registrasi);
            redirect(base_url("index.php/c_user/data_diri"));
        }
    }
    function ubah_data_diri()
    {
        $id=$this->session->userdata('nomor_registrasi');
        $data['header']=$this->data_model->data_diri_siswa($id);
		$data['total']=$this->data_model->total_tagihan($id);
        $this->load->view('user/header',$data);
        $this->load->view('user/ubah_data_diri');
        $this->load->view('user/footer');
    }
    function do_upload() {
            // setting konfigurasi upload
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']=TRUE;
            // load library upload_path
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('bukti_bayar')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = $this->upload->data();
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            $this->data_model->upload_media_pembayaran();
            redirect(base_url("index.php/c_user/upload_bukti_transfer"));
        }

    }
    function upload_bukti_transfer($id)
    {// setting konfigurasi upload
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('bukti_bayar')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = array('upload_data' =>$this->upload->data());
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            $nomo=$this->session->userdata('nomor_registrasi');
            $data['id_media']=$this->data_model->no_media_otomatis();
            $data['jenis_media']='bukti_bayar'.$nomo;
            $data['media']=$result['upload_data']['file_name'];
            $this->data_model->upload_media('tb_media',$data);
            $this->data_model->update_status_konfirmasi($id,$data['id_media']);
            redirect(base_url("index.php/c_user/paket_pendidikan_diambil"));
        }
    }
    function paket_pendidikan_diambil()
    {
        $id=$this->session->userdata('nomor_registrasi');
        $data['data_paket_pendidikan']=$this->data_model->Paket_pendidikan_diambil($id);
        $nomor_registrasi=$this->session->userdata('nomor_registrasi');
		$data['total']=$this->data_model->total_tagihan($nomor_registrasi);
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        $this->load->view('user/header',$data);
        $this->load->view('user/paket_pendidikan_diambil');
        $this->load->view('user/footer');
    }
    function simpan_paket_dipilih($id)
    {
		$status=$this->input->post('status');
		$data_paket=$this->data_model->edit_paket_bimbel($id);
		foreach ($data_paket->result() as $value)
		{
			$total_biaya=$value->total_biaya;
		}
		if($status=='Cicilan')
		{
			$data['no_registrasi']=$this->session->userdata('nomor_registrasi');
			$data['id_pendidikan ']=$id;
			$data['status']=$this->input->post('status');
			$data['status_konfirmasi']="Menunggu Cicilan";
			$data['bukti_bayar']="Tidak Ada";
			$data['tgl_di_ambil']=date('dmY');
			$data['tgl_lunas']="-";
			$data['total_tagihan']=$total_biaya;
			//print_r($data);
			//$data2['id_total_tagihan']
			//$this->data_model->Simpan_cicilan($data2);
			$this->data_model->Simpan_paket_dipilih($data);
			redirect(base_url("index.php/c_user/bayar_tagihan"));
		}else{
			$data['no_registrasi']=$this->session->userdata('nomor_registrasi');
			$data['id_pendidikan ']=$id;
			$data['status']=$this->input->post('status');
			$data['status_konfirmasi']="Menunggu Pembayaran";
			$data['bukti_bayar']="Tidak Ada";
			$data['tgl_di_ambil']=date('dmY');
			$data['tgl_lunas']="-";
			$data['total_tagihan']=$total_biaya;
			//print_r($data);
			$this->data_model->Simpan_paket_dipilih($data);
			redirect(base_url("index.php/c_user/paket_pendidikan_diambil"));
		}

    }
    function paket_pendidikan_aktif()
    {
        $id=$this->session->userdata('nomor_registrasi');
        $data['data_paket_pendidikan']=$this->data_model->Paket_pendidikan_aktif($id);
        $nomor_registrasi=$this->session->userdata('nomor_registrasi');
		$data['total']=$this->data_model->total_tagihan($nomor_registrasi);
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        $this->load->view('user/header',$data);
        $this->load->view('user/paket_pendidikan_aktif');
        $this->load->view('user/footer');
    }
    function view_invoice($id)
    {
    	$nomor_registrasi=$this->session->userdata('nomor_registrasi');
        $data['paket_pendidikan']=$this->data_model->Paket_dipilih($id);
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        $this->load->view('user/header',$data);
        $this->load->view('user/invoice_biaya');
        $this->load->view('user/footer');
    }

    function index(){
    	$this->load->view('user/formulir_data_diri');
    	$this->load->view('user/footer');
    }
    function bayar_tagihan(){
		$id=$this->session->userdata('nomor_registrasi');
		$data['data_paket_pendidikan']=$this->data_model->Paket_pendidikan_cicil($id);
		$nomor_registrasi=$this->session->userdata('nomor_registrasi');
		$data['total']=$this->data_model->total_tagihan($nomor_registrasi);
		$data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        $this->load->view('user/header',$data);
        $this->load->view('user/bayar_tagihan');
        $this->load->view('user/footer');
    }
    function beranda(){
        $nomor_registrasi=$this->session->userdata('nomor_registrasi');
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        $data['total']=$this->data_model->total_tagihan($nomor_registrasi);
        //print_r($data['total']);
        $this->load->view('user/header',$data);
        $this->load->view('user/beranda');
        $this->load->view('user/footer');
    }
    function simpan_data_diri()
    {
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = array('upload_data' =>$this->upload->data());
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
            $no_registrasi=$this->session->userdata('nomor_registrasi');
            $media['id_media']=$this->data_model->no_media_otomatis();
            $media['jenis_media']='foto_siswa'.$no_registrasi;
            $media['media']=$result['upload_data']['file_name'];
            $siswa['no_registrasi']=$no_registrasi;
            $siswa['jenis_kelamin']=$this->input->post('jenis_kelamin');
            $siswa['tempat_lahir']=$this->input->post('tempat_lahir');
            $siswa['tgl_lahir']=$this->input->post('tgl_lahir');
            $siswa['alamat']=$this->input->post('alamat');
            $siswa['tlp']=$this->input->post('telp');
            $siswa['whatsapp']=$this->input->post('whatsapp');
            $siswa['email']=$this->input->post('email');
            $siswa['bbm']=$this->input->post('bbm');
            $siswa['foto']=$media['id_media'];
            $sekolah['no_registrasi']=$no_registrasi;
            $sekolah['nm_sekolah']=$this->input->post('nama_sekolah');
            $sekolah['alamat_sekolah']=$this->input->post('alamat_sekolah');
            $sekolah['kelas']=$this->input->post('kelas');
            $ortu['no_registrasi']=$no_registrasi;
            $ortu['nama_ayah']=$this->input->post('nm_ayah');
            $ortu['pekerjaan_ayah']=$this->input->post('pk_ayah');
            $ortu['nama_ibu']=$this->input->post('nm_ibu');
            $ortu['pekerjaan_ibu']=$this->input->post('pekerjaan_ibu');
            $ortu['alamat_orangtua']=$this->input->post('alamat_orangtua');
            $ortu['hp']=$this->input->post('tlp_ortu');
            $ortu['Whatsapp_ortu']=$this->input->post('Whatsapp_ortu');
            $status_registrasi="Sudah";
            $this->data_model->update_status_registrasi('tb_user',$no_registrasi,$status_registrasi);
            $this->data_model->Simpan_Siswa('tb_siswa',$siswa);
            $this->data_model->Simpan_Sekolah('tb_sekolah',$sekolah);
            $this->data_model->Simpan_Ortu('tb_orangtua',$ortu);
            $this->data_model->upload_media('tb_media',$media
        );
            redirect(base_url('index.php/c_user/beranda'));
        }
    }
    function data_diri_siswa(){
       $nomor_registrasi=$this->session->userdata('nomor_registrasi');
       $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
       $data['data_siswa']=$this->data_model->data_diri_siswa($nomor_registrasi);
       $data['total']=$this->data_model->total_tagihan($nomor_registrasi);
       $this->load->view('user/header',$data);
       $this->load->view('user/data_diri');
       $this->load->view('user/footer');
   }
   function paket_pendidikan_tersedia()
   {

    $nomor_registrasi=$this->session->userdata('nomor_registrasi');
    $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
    $data['total']=$this->data_model->total_tagihan($nomor_registrasi);
    $data['data_paket_pendidikan']=$this->data_model->detail_paket_pendidikan();
    $this->load->view('user/header',$data);
    $this->load->view('user/paket_pendidikan_tersedia');
    $this->load->view('user/footer');
}
}
?>
