<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * summary
 */
class C_admin extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    }

    function simpan_password($id)
    {
        $data['password']=md5($this->input->post('password'));
        $this->data_model->update_media('nomor_registrasi',$id,'tb_user',$data);
        redirect(base_url('index.php/c_admin/data_siswa'));
    }
    function edit_data_siswa($id)
    {
        $data['data']=$this->data_model->edit_data_siswa('nomor_registrasi',$id,'tb_user');
        $this->load->view('admin/header');
        $this->load->view('admin/edit_data_password',$data);
        $this->load->view('admin/footer');
    }
    function hapus($id)
    {
        $this->data_model->hapus('tb_komentar',$id,'id_komentar');
        redirect(base_url('index.php/c_admin/list_komentar'));
    }
    function list_komentar()
    {
        $data['data']=$this->data_model->data('tb_komentar');
        $this->load->view('admin/header');
        $this->load->view('admin/list_komentar',$data);
        $this->load->view('admin/footer');
    }
    function list_tenaga_pengajar()
    {
        $data['data']=$this->data_model->list_tenaga_pengajar();
        $this->load->view('admin/header');
        $this->load->view('admin/list_tenaga_pengajar',$data);
        $this->load->view('admin/footer');
    }
    function hapus_data_staf($id)
    {
        $data=$this->data_model->detail_data_staf_hapus($id);
        foreach ($data->result() as $value) {
            $id_staf=$value->id_staf;
            $foto=$value->foto;
        }
        unlink("./media/$foto");
        $this->data_model->hapus_data_staf($id_staf);
        redirect(base_url('index.php/c_admin/list_tenaga_pengajar'));
    }
    function simpan_data_staf()
    {
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']='500000';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = array('upload_data' =>$this->upload->data());
            $media['id_staf']='';
            $media['nama']=$this->input->post('nama');
            $media['jabatan']=$this->input->post('jabatan');
            $media['foto']=$result['upload_data']['file_name'];
            $this->data_model->simpan_data_fasilitas('tb_staf',$media);
            redirect(base_url('index.php/c_admin/list_tenaga_pengajar'));
        }
    }
    function tambah_tenaga_pengajar()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/tambah_tenaga_pengajar');
        $this->load->view('admin/footer');
    }
    function hapus_fasilitas($id)
    {
        $data=$this->data_model->detail_fasilitas_hapus($id);
        foreach ($data->result() as $value) {
            $id_fasilitas=$value->id_fasilitas;
            $foto=$value->foto;
        }
        unlink("./media/$foto");
        $this->data_model->hapus_data_fasilitas($id_fasilitas);
        redirect(base_url('index.php/c_admin/tambah_fasilitas')); 
    }
    function simpan_data_fasilitas()
    {
        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']='500000';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $result = array('upload_data' =>$this->upload->data());
            $media['id_fasilitas']='';
            $media['type']=$this->input->post('type');
            $media['foto']=$result['upload_data']['file_name'];
            $media['keterangan']=$this->input->post('keterangan');
            $this->data_model->simpan_data_fasilitas('tb_fasilitas',$media);
            redirect(base_url('index.php/c_admin/tambah_data_fasilitas'));
        }

    }
    function tambah_data_fasilitas()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/tambah_fasilitas');
        $this->load->view('admin/footer');
    }
    function tambah_fasilitas()
    {
        $data['data']=$this->data_model->detail_fasilitas();
        $this->load->view('admin/header');
        $this->load->view('admin/list_fasilitas',$data);
        $this->load->view('admin/footer');
    }
    function ubah_background_beranda()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/ubah_background_beranda');
        $this->load->view('admin/footer');
    }
    function simpan_ubah_background_beranda()
    {

        $config['upload_path'] = './media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']='500000';
        $config['encrypt_name']=TRUE;
            // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            $error = $this->upload->display_errors();
                // menampilkan pesan error
            print_r($error);
        } else {
            $data=$this->data_model->ambil_data_by_id('tb_media','id_media','MEDIA0003');
            foreach ($data->result() as $value) {
                $id_media=$value->id_media;
                $media=$value->media;
                unlink("./media/$media");
            }
            $result = array('upload_data' =>$this->upload->data());
            $foto['media']=$result['upload_data']['file_name'];
            $this->data_model->update_media('id_media','MEDIA0003','tb_media',$foto);
            redirect(base_url('index.php/c_admin/ubah_background_beranda'));
        }
    }
    function hapus_data_siswa($id)
    {
        $data=$this->data_model->data_diri_siswa($id);
        foreach ($data->result() as $value) {
            $nomor_registrasi=$value->nomor_registrasi;
            $id_siswa=$value->id_siswa;
            $id_sekolah=$value->id_sekolah;
            $id_orangtua=$value->id_orangtua;
            $id_media=$value->id_media;
            $media=$value->media;
        }
        unlink("./media/$media");
        $this->data_model->hapus_data_siswa($nomor_registrasi,$id_siswa,$id_sekolah,$id_orangtua,$id_media);
        redirect(base_url('index.php/c_admin/data_siswa'));
    }
    function simpan_edit_paket_bimbel($id)
    {
        $data['nama_paket']=$this->input->post('nama_paket');
        $data['total_biaya']=$this->input->post('total_biaya');
        $data['rincian']=$this->input->post('rincian');
        $data['id_paket']=$id;
        $this->data_model->simpan_edit_paket_bimbel('tb_paket_pendidikan',$data,'id_paket');
        redirect(base_url('index.php/c_admin/admin_peket_pendidikan_tersedia'));
    }
    function edit_paket_bimbel($id)
    {
        $data['data']=$this->data_model->edit_paket_bimbel($id);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_paket_bimbel',$data);
        $this->load->view('admin/footer');
    }
    function hapus_paket_bimbel($id)
    {
        $this->data_model->hapus_paket_bimbel($id);
        redirect(base_url('index.php/c_admin/admin_peket_pendidikan_tersedia'));
    }
    function simpan_edit_web($id)
    {
        $data['koding']=$this->input->post('web');
        $this->data_model->Simpan_edit_web($data);
        redirect(base_url('index.php/c_admin/edit_web'));
    }
    function edit_web($id)
    {
        $data['data']=$this->data_model->Edit_web($id);
        $this->load->view('admin/header');
        $this->load->view('admin/edit_web',$data);
        $this->load->view('admin/footer');
    }
    function beranda()
    {
        //$data['hitung']=$this->data_model->hitung_jumlah_siswa();
        //printf($data['hitung']);
        $data['total_siswa']=$this->data_model->hitung_jumlah_siswa();
        $data['total_paket']=$this->data_model->hitung_jumlah_paket();
        $this->load->view('admin/header');
        $this->load->view('admin/beranda',$data);
        $this->load->view('admin/footer');	
    }
    function data_siswa()
    {   
        $data['detail_siswa']=$this->data_model->admin_data_siswa();
        $this->load->view('admin/header');
        $this->load->view('admin/table_siswa',$data);
        $this->load->view('admin/footer');
    }
    function admin_tambah_peket_pendidikan()
    {
        $this->load->view('admin/header');
        $this->load->view('admin/tambah_paket_pendidikan');
        $this->load->view('admin/footer');
    }
    function admin_peket_pendidikan_tersedia()
    {
        $data['data_paket_pendidikan']=$this->data_model->detail_paket_pendidikan();
        $this->load->view('admin/header');
        $this->load->view('admin/paket_pendidikan_tersedia',$data);
        $this->load->view('admin/footer');
    }
    function simpan_paket_pendidikan()
    {
        $paket['id_paket']=$this->data_model->no_paket_otomatis();
        $paket['nama_paket']=$this->input->post('nama_paket');
        $paket['total_biaya']=$this->input->post('total_biaya');
        $paket['rincian']=$this->input->post('rincian');
        $this->data_model->Simpan_paket_pendidikan('tb_paket_pendidikan',$paket);
        redirect(base_url('index.php/c_admin/admin_peket_pendidikan_tersedia'));
        echo $this->session->set_flashdata('msg','Paket Pendidikan Berhasil di Tambahkan');

    }
}
