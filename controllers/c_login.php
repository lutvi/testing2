<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {
	 public function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in') !==TRUE){
			redirect('index.php/controller/login');
		}
	}

	 function admin()
	{
		if($this->session->userdata('level')==='admin'){
			redirect('index.php/c_admin/beranda');
		}else {
			echo 'Access Denied';
		}
	}
	 function siswa()
	{
		if($this->session->userdata('level')==='siswa')
		{
			if ($this->session->userdata('status_registrasi')==='Belum') {
			$this->load->view('user/formulir_data_diri');
			$this->load->view('user/footer');
			}
			else {
			$nomor_registrasi=$this->session->userdata('nomor_registrasi');
        $data['header']=$this->data_model->data_diri_siswa($nomor_registrasi);
        redirect(base_url('index.php/c_user/beranda'));
//			$this->load->view('user/header',$data);
//			$this->load->view('user/beranda');
//			$this->load->view('user/footer');
			}
		}
		else {
			echo 'Access Denied';
		}
	}
}
